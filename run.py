import os
import subprocess

if __name__ == "__main__":
    args = {}

    for arg in os.sys.argv[1:]:
        key, val = arg.split("=")
        if val:
            args[key] = val

    command = ["codespell", "-f", "-C"]

    if to_ignore := args.get("TO_IGNORE"):
        command.extend(["-L", to_ignore])

    if to_skip := args.get("TO_SKIP"):
        command.extend(["-S", to_skip])

    to_check = args.get("TO_CHECK", ".")
    command.append(to_check)

    subprocess.call(command, cwd=os.path.abspath(__file__))
